/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is not neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! namespace exports */
/*! exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__, __webpack_require__.n, __webpack_require__.r, __webpack_exports__, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _scss_demo_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./scss/demo.scss */ \"./src/scss/demo.scss\");\n/* harmony import */ var _scss_style_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./scss/style.scss */ \"./src/scss/style.scss\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ \"jquery\");\n/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\n\n\n\nvar regex = /^[a-z]+$/i;\n\nvar RadialProgressDemo = /*#__PURE__*/function () {\n  function RadialProgressDemo() {\n    _classCallCheck(this, RadialProgressDemo);\n\n    jquery__WEBPACK_IMPORTED_MODULE_2___default()(\"#setProgress\").val(\"\");\n    jquery__WEBPACK_IMPORTED_MODULE_2___default()(\"#setLink\").val(\"\");\n    jquery__WEBPACK_IMPORTED_MODULE_2___default()(\"#setText\").val(\"\");\n    jquery__WEBPACK_IMPORTED_MODULE_2___default()(\"#setAddText\").val(\"\");\n    this.bindEvents();\n  }\n\n  _createClass(RadialProgressDemo, [{\n    key: \"bindEvents\",\n    value: function bindEvents() {\n      jquery__WEBPACK_IMPORTED_MODULE_2___default()(\"#demo-form-submit\").on(\"click\", function (e) {\n        var sizeValue = jquery__WEBPACK_IMPORTED_MODULE_2___default()(\"#setSize\").val();\n        var thicknessValue = jquery__WEBPACK_IMPORTED_MODULE_2___default()(\"#setThickness\").val();\n        var progressValue = jquery__WEBPACK_IMPORTED_MODULE_2___default()(\"#setProgress\").val();\n        var speedValue = jquery__WEBPACK_IMPORTED_MODULE_2___default()(\"#setSpeed\").val();\n        var linkValue = jquery__WEBPACK_IMPORTED_MODULE_2___default()(\"#setLink\").val();\n        var setTextValue = jquery__WEBPACK_IMPORTED_MODULE_2___default()(\"#setText\").val();\n        var setAddTextValue = jquery__WEBPACK_IMPORTED_MODULE_2___default()(\"#setAddText\").val();\n\n        if (regex.test(progressValue)) {\n          return;\n        } else {\n          if (progressValue === \"\" || progressValue > 100 || progressValue < 0) {\n            return;\n          } else {\n            jquery__WEBPACK_IMPORTED_MODULE_2___default()(\".radial-progress\").attr('class', 'radial-progress').addClass(\"--\".concat(sizeValue));\n            jquery__WEBPACK_IMPORTED_MODULE_2___default()(\".radial-progress\").addClass(\"--\" + thicknessValue).addClass(\"--speed-\".concat(speedValue));\n            jquery__WEBPACK_IMPORTED_MODULE_2___default()(\".radial-progress__link\").attr('href', linkValue);\n            jquery__WEBPACK_IMPORTED_MODULE_2___default()(\".radial-progress__heading\").text(setTextValue);\n            jquery__WEBPACK_IMPORTED_MODULE_2___default()(\".radial-progress__body\").text(setAddTextValue);\n            setTimeout(function () {\n              jquery__WEBPACK_IMPORTED_MODULE_2___default()(\".radial-progress\").addClass(\"--rotate-\".concat(progressValue));\n            }, 500);\n          }\n        }\n\n        e.preventDefault();\n      });\n    }\n  }]);\n\n  return RadialProgressDemo;\n}();\n\nnew RadialProgressDemo();\n\n//# sourceURL=webpack://radial-progress/./src/index.js?");

/***/ }),

/***/ "./src/scss/demo.scss":
/*!****************************!*\
  !*** ./src/scss/demo.scss ***!
  \****************************/
/*! namespace exports */
/*! exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__.r, __webpack_exports__, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack://radial-progress/./src/scss/demo.scss?");

/***/ }),

/***/ "./src/scss/style.scss":
/*!*****************************!*\
  !*** ./src/scss/style.scss ***!
  \*****************************/
/*! namespace exports */
/*! exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__.r, __webpack_exports__, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack://radial-progress/./src/scss/style.scss?");

/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/*! dynamic exports */
/*! export __esModule [maybe provided (runtime-defined)] [no usage info] [provision prevents renaming (no use info)] */
/*! other exports [maybe provided (runtime-defined)] [no usage info] */
/*! runtime requirements: module */
/***/ ((module) => {

eval("module.exports = jQuery;\n\n//# sourceURL=webpack://radial-progress/external_%22jQuery%22?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => module['default'] :
/******/ 				() => module;
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop)
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	// startup
/******/ 	// Load entry module
/******/ 	__webpack_require__("./src/index.js");
/******/ 	// This entry module used 'exports' so it can't be inlined
/******/ })()
;