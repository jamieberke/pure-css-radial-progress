import "./scss/demo.scss";
import "./scss/style.scss";
import $ from 'jquery';

const regex = /^[a-z]+$/i;

class RadialProgressDemo {
  constructor() {
    $("#setProgress").val("");
    $("#setLink").val("");
    $("#setText").val("");
    $("#setAddText").val("");
    this.bindEvents();
  }
  
  bindEvents() {
    $("#demo-form-submit").on("click", (e) => {
      const sizeValue = $("#setSize").val();
      const thicknessValue = $("#setThickness").val();
      const progressValue = $("#setProgress").val();
      const speedValue = $("#setSpeed").val();
      const linkValue = $("#setLink").val();
      const setTextValue = $("#setText").val();
      const setAddTextValue = $("#setAddText").val();
      if (regex.test(progressValue)) {
        return;
      } else {
        if (progressValue === "" || progressValue > 100 || progressValue < 0) {
          return;
        } else {
          $(".radial-progress").attr('class', 'radial-progress').addClass(`--${sizeValue}`);
          $(".radial-progress").addClass("--" + thicknessValue).addClass(`--speed-${speedValue}`);
          $(".radial-progress__link").attr('href', linkValue);
          $(".radial-progress__heading").text(setTextValue);
          $(".radial-progress__body").text(setAddTextValue);
          setTimeout(function () {
            $(".radial-progress").addClass(`--rotate-${progressValue}`);
          }, 500);
        }
      }
      e.preventDefault();
    });
  }
}

new RadialProgressDemo();
