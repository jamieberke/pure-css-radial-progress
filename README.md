# Pure CSS Radial Progress

#### Description
This component is a pure CSS/HTML radial progress component that can be used to show progress (e.g. percentages). It uses SCSS to produce rotation and speed classes for the users preset values and uses transitions to animate itself. The component is configurable in the following ways:

- Rotation value
- Rotation Speed (1-9 seconds)
- Rotation thickness
- Rotation size
- Component link (using `href`)
- Component main text (e.g. a percentage value - 75%)
- Component additional text (text to be used underneath the main text)

#### How to use the demo

1. Access the project route
2. Run `npm install`
3. Run `npm run start`

Interchangeable classes to control the animation/appearance are applied on the component wrapper `radial-progress`. The component must have all of these classes to animate/appear correctly.

- `--rotate-[1-100]`
- `--[thick || thin]`
- `--[small || medium || large]`
- `--speed-[1-9]`

#### Integrating the component in your project

You will need to build the component to get the compiled files. You can do this by accessing the project route and running either of the following scripts:

- `npm run [dev|start]` - this will compile the files in dev mode
- `npm run build` - this will compile the files in production mode

To use this component all you need is the following files:

- `build/main.css`

You will also need the following HTML. Remember that any configurable classes will need to be added on the `radial-progress` element. How you choose to start the animation and configure the component is entirely up to you.

```
<div class="radial-progress">
  <a class="radial-progress__link" href=""></a>
  <div class="radial-progress__background"></div>
  <div class="radial-progress__info">
    <img class="radial-progress__img" src=""/>
    <span class="radial-progress__heading"></span>
    <span class="radial-progress__body"></span>
  </div>
  <div class="radial-progress__dial-container --left">
    <div class="radial-progress__dial-container-wedge"></div>
  </div>
  <div class="radial-progress__dial-container --right">
    <div class="radial-progress__dial-container-wedge"></div>
  </div>
</div>
```

#### Notes
Any JavaScript used is purely for demonstrative purposes to allow the component to be configured by the user and see how it behaves under different configurations and class names. This demo contains CSS and JavaScript dependencies which can be removed and are only used from demonstrative purposes.
