const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const plugins = [
  new HtmlWebpackPlugin({
    template: path.resolve(__dirname, "src", "index.html"),
  }),
  new MiniCssExtractPlugin(),
];

module.exports = {
  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        include: /src/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader",
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
    ],
  },
  output: {
    path: path.resolve(__dirname, "build"),
  },
  optimization: {
    splitChunks: { chunks: "all" },
  },
  plugins,
  devServer: {
    open: true,
  },
  externals: {
    jquery: "jQuery",
  },
};
